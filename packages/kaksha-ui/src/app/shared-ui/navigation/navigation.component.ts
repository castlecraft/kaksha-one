import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';
import { Router, NavigationEnd } from '@angular/router';
import { StorageService } from '../../common/services/storage/storage.service';
import {
  ISSUER_URL,
  APP_URL,
  COMMUNICATION,
  IDENTITY_PROVIDER,
  LIST,
} from '../../constants/storage';
import { IDTokenClaims } from '../../interfaces/id-token-claims.interfaces';
import { ADMINISTRATOR } from '../../constants/roles';
import { NEW_ID } from '../../constants/common';
import { ADD_BUTTON_ROUTES } from '../../constants/url-paths';
import { AppService } from '../../app.service';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  tokenIsValid: boolean;
  loggedIn: boolean;
  routeArray: string[];
  hideFAB: boolean;
  isCommunicationEnabled: boolean;
  isIdentityProviderAvailable: boolean;
  fabButtonEnable: boolean;
  userPicture: string = '';
  userName: string = '';
  userEmail: string = '';

  constructor(
    private breakpointObserver: BreakpointObserver,
    private oauthService: OAuthService,
    private router: Router,
    private storageService: StorageService,
    private appService: AppService,
  ) {
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.routeArray = route.url.split('/');
        // add routes to ADD_BUTTON_ROUTES array to enable add button on your listing routes.
        if (
          ADD_BUTTON_ROUTES.includes(
            this.routeArray[this.routeArray.length - 1],
          )
        ) {
          this.fabButtonEnable = true;
          return;
        }
        this.fabButtonEnable = false;
      });
  }

  ngOnInit(): void {
    this.oauthService.events.subscribe(({ type }: OAuthEvent) => {
      // Silent Refresh
      switch (type) {
        case 'token_received':
          this.setUserSession();
          this.router.navigate(['home']);
          break;
      }
    });

    this.setUserSession();
    this.appService.getProfile().subscribe({
      next: (profile: any) => {
        this.userEmail = profile.email;
        this.userName = profile.name;
        this.userPicture = profile.picture;
      },
    });
    try {
      this.isIdentityProviderAvailable = this.storageService.getServiceURL(
        IDENTITY_PROVIDER,
      );
    } catch (error) {
      this.isIdentityProviderAvailable = false;
    }

    try {
      this.isCommunicationEnabled = JSON.parse(
        localStorage.getItem(COMMUNICATION),
      );
    } catch (error) {
      this.isCommunicationEnabled = false;
    }
  }

  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    const logOutUrl =
      this.storageService.getInfo(ISSUER_URL) +
      '/auth/logout?redirect=' +
      this.storageService.getInfo(APP_URL);
    this.storageService.clearInfoLocalStorage();
    this.oauthService.logOut();
    this.tokenIsValid = false;
    window.location.href = logOutUrl;
  }

  setUserSession() {
    const idClaims: IDTokenClaims = this.oauthService.getIdentityClaims() || {
      roles: [],
    };
    this.tokenIsValid = idClaims.roles.includes(ADMINISTRATOR);
    this.loggedIn = this.oauthService.hasValidAccessToken();
  }

  addModel() {
    // TODO: make it better in UI/UX
    if (this.routeArray.includes(LIST)) {
      this.routeArray[this.routeArray.indexOf(LIST)] = this.routeArray[
        this.routeArray.length - 1
      ];
      this.routeArray[this.routeArray.length - 1] = NEW_ID;
      this.router.navigate(this.routeArray);
    }
  }
}
