import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { TeacherDto } from '../../../app/education/services/teacher/teacher.dto';
import { TeacherService } from '../../../app/education/services/teacher/teacher.service';
import { ChartOptions, ChartType } from 'chart.js';
import { Color, Label, SingleDataSet } from 'ng2-charts';
import { mergeMap, switchMap, toArray } from 'rxjs/operators';
import { from, of } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  chartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    cutoutPercentage: 75,
    legend: {
      display: false,
    },
  };
  teacher: TeacherDto[] = [];
  teacherInfo: TeacherDto = {};
  doughnutChartLabels: Label[] = ['Completed', 'Remaining'];
  doughnutChartData: SingleDataSet = [];
  doughnutChartType: ChartType = 'doughnut';
  doughnutChartColors: Color[] = [
    {
      backgroundColor: ['#3C43D9', '#D1D3F6'],
      borderWidth: 0,
    },
  ];
  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private teacherService: TeacherService,
  ) {}

  ngOnInit() {
    if (this.oauthService.hasValidAccessToken()) {
      this.router.navigate(['home']);
    }
    this.teacherService
      .fetchTeacherByEmail()
      .pipe(
        switchMap((teacherData: TeacherDto) => {
          this.teacherInfo = teacherData;
          return from(teacherData.subjects);
        }),
        mergeMap(subject => {
          return from(subject.divisions).pipe(
            mergeMap(data => {
              return of({
                ...this.teacherInfo,
                subject: { ...subject, division: data },
              });
            }),
          );
        }),
        toArray(),
      )
      .subscribe({
        next: (teacher: TeacherDto[]) => {
          Object.assign(this.teacher, teacher);
        },
      });
  }
}
