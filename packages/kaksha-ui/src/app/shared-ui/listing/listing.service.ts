import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StorageService } from '../../common/services/storage/storage.service';
import { APP_URL } from '../../constants/storage';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { AUTHORIZATION, API_PREFIX } from '../../constants/common';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  handleError: HandleError;

  constructor(
    private storageService: StorageService,
    httpErrorHandler: HttpErrorHandler,
    private readonly http: HttpClient,
    private readonly oauth2: OAuthService,
  ) {
    this.handleError = httpErrorHandler.createHandleError('ListingService');
  }

  findModels(
    model: string,
    filter = '',
    sortOrder = 'asc',
    pageNumber = 0,
    pageSize = 10,
  ) {
    const baseUrl = this.storageService.getInfo(APP_URL);

    const url = `${baseUrl}/${API_PREFIX}/${model}/v1/list`;
    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filter)
      .set('sort', sortOrder);
    return this.http.get(url, {
      params,
      headers: {
        [AUTHORIZATION]: this.oauth2.authorizationHeader(),
      },
    });
  }
}
