import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationComponent } from './components/organization/organization.component';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { QuizComponent } from './components/quiz/quiz.component';
import { OrganizationService } from './services/organization/organization.service';
import { TeacherComponent } from './components/teacher/teacher.component';
import { TeacherService } from './services/teacher/teacher.service';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import {
  StandardComponent,
  SubjectDialog,
} from './components/standard/standard.component';
import { StandardService } from './services/standard/standard.service';
import { TeacherSubjectDialog } from './components/teacher/teacher-subject-dialog.component';
import { QuestionDialogueComponent } from './components/quiz/quiz.component';
import {
  SubjectComponent,
  TopicDialog,
} from './components/subject/subject.component';
import { KakshaComponent } from './components/kaksha/kaksha.component';

@NgModule({
  declarations: [
    OrganizationComponent,
    QuizComponent,
    TeacherComponent,
    DeleteDialogComponent,
    StandardComponent,
    SubjectDialog,
    TeacherSubjectDialog,
    QuestionDialogueComponent,
    SubjectComponent,
    TopicDialog,
    KakshaComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [CommonModule, SharedImportsModule],
  providers: [OrganizationService, TeacherService, StandardService],
})
export class EducationModule {}
