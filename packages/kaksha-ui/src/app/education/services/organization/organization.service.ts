import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AUTHORIZATION } from '../../../constants/common';
import { FETCH_ORG_ENDPOINT } from '../../../constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class OrganizationService {
  constructor(
    private readonly http: HttpClient,
    private readonly oauth2: OAuthService,
  ) {}

  fetchOrganization() {
    return this.http.get<{
      name: string;
      email: string;
    }>(FETCH_ORG_ENDPOINT, {
      headers: {
        [AUTHORIZATION]: this.oauth2.authorizationHeader(),
      },
    });
  }
}
