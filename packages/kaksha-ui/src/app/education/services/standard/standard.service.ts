import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  GET_STANDARD_ENDPOINT,
  GET_ALL_SUBJECTS_ENDPOINT,
  CREATE_STANDARD_ENDPOINT,
  DELETE_SUBJECT_ENDPOINT,
  UPDATE_STANDARD_ENDPOINT,
  CREATE_SUBJECT_ENDPOINT,
  UPDATE_SUBJECT_ENDPOINT,
  GET_SUBJECT_ENDPOINT,
  GET_ALL_TOPIC_ENDPOINT,
  DELETE_TOPIC_ENDPOINT,
  CREATE_TOPIC_ENDPOINT,
  UPDATE_TOPIC_ENDPOINT,
  GET_SUBJECTS_LIST_ENDPOINT,
  GET_STANDARD_LIST_ENDPOINT,
} from '../../../constants/url-paths';
import { ACCESS_TOKEN, API_PREFIX } from '../../../constants/storage';
import { AUTHORIZATION } from '../../../constants/common';
import { StandardInterface } from '../../components/standard/standard.component';
import { Observable } from 'rxjs';
import {
  SubjectInterface,
  TopicsInterface,
} from '../../components/subject/subject.component';

@Injectable({
  providedIn: 'root',
})
export class StandardService {
  constructor(private readonly http: HttpClient) {}

  getStandard(uuid: string): Observable<StandardInterface> {
    return this.http.get(`${API_PREFIX}${GET_STANDARD_ENDPOINT}${uuid}`, {
      headers: this.getHeaders(),
    });
  }

  getSubject(uuid: string) {
    return this.http.get(`${API_PREFIX}${GET_SUBJECT_ENDPOINT}${uuid}`, {
      headers: this.getHeaders(),
    });
  }

  getTopics(subjectName: string, standardName: string) {
    const params = new HttpParams()
      .set('standardName', standardName)
      .set('subjectName', subjectName);
    return this.http.get(`${API_PREFIX}${GET_ALL_TOPIC_ENDPOINT}`, {
      params,
      headers: this.getHeaders(),
    });
  }

  deleteTopic(topic: TopicsInterface) {
    return this.http.post(
      `${API_PREFIX}${DELETE_TOPIC_ENDPOINT}/${topic.uuid}`,
      {},
      { headers: this.getHeaders() },
    );
  }

  saveTopic(topic: TopicsInterface) {
    if (topic.uuid) return this.updateTopic(topic);
    return this.http.post(`${API_PREFIX}${CREATE_TOPIC_ENDPOINT}`, topic, {
      headers: this.getHeaders(),
    });
  }

  updateTopic(topic: TopicsInterface) {
    return this.http.post(`${API_PREFIX}${UPDATE_TOPIC_ENDPOINT}`, topic, {
      headers: this.getHeaders(),
    });
  }

  getSubjects(standardName: string) {
    return this.http.get(
      `${API_PREFIX}${GET_ALL_SUBJECTS_ENDPOINT}${standardName}`,
      { headers: this.getHeaders() },
    );
  }

  saveStandard(standard: StandardInterface) {
    if (standard.uuid) return this.updateStandard(standard);
    return this.http.post(
      `${API_PREFIX}${CREATE_STANDARD_ENDPOINT}`,
      standard,
      { headers: this.getHeaders() },
    );
  }

  updateStandard(standard: StandardInterface) {
    return this.http.post(
      `${API_PREFIX}${UPDATE_STANDARD_ENDPOINT}`,
      standard,
      { headers: this.getHeaders() },
    );
  }

  saveSubject(subject: SubjectInterface) {
    if (subject.uuid) return this.updateSubject(subject);
    return this.http.post(`${API_PREFIX}${CREATE_SUBJECT_ENDPOINT}`, subject, {
      headers: this.getHeaders(),
    });
  }

  deleteSubject(subject: SubjectInterface) {
    return this.http.post(
      `${API_PREFIX}${DELETE_SUBJECT_ENDPOINT}/${subject.uuid}`,
      {},
      { headers: this.getHeaders() },
    );
  }

  updateSubject(subject: SubjectInterface) {
    return this.http.post(`${API_PREFIX}${UPDATE_SUBJECT_ENDPOINT}`, subject, {
      headers: this.getHeaders(),
    });
  }

  getSubjectList() {
    return this.http.get(`${API_PREFIX}${GET_SUBJECTS_LIST_ENDPOINT}`, {
      headers: this.getHeaders(),
    });
  }

  getStandardList() {
    return this.http.get(`${API_PREFIX}${GET_STANDARD_LIST_ENDPOINT}`, {
      headers: this.getHeaders(),
    });
  }

  getHeaders() {
    return {
      [AUTHORIZATION]: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
    };
  }
}
