import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHORIZATION } from '../../../constants/common';
import { ACCESS_TOKEN, API_PREFIX } from '../../../constants/storage';
import {
  CREATE_QUIZ_ENDPOINT,
  UPDATE_QUIZ_ENDPOINT,
  GET_QUIZ_ENDPOINT,
} from '../../../constants/url-paths';
import { QuizInterface } from '../../components/quiz/quiz.component';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  constructor(private readonly http: HttpClient) {}

  saveQuiz(quiz: QuizInterface) {
    if (quiz.uuid) return this.updateQuiz(quiz);
    return this.http.post(`${API_PREFIX}${CREATE_QUIZ_ENDPOINT}`, quiz, {
      headers: this.getHeaders(),
    });
  }

  updateQuiz(quiz: QuizInterface) {
    return this.http.post(`${API_PREFIX}${UPDATE_QUIZ_ENDPOINT}`, quiz, {
      headers: this.getHeaders(),
    });
  }
  getHeaders() {
    return {
      [AUTHORIZATION]: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
    };
  }

  getQuiz(uuid: string) {
    return this.http.get(`${API_PREFIX}${GET_QUIZ_ENDPOINT}${uuid}`, {
      headers: this.getHeaders(),
    });
  }
}
