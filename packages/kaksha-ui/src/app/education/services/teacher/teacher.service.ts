import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

import {
  CREATE_TEACHER_ENDPOINT,
  DELETE_TEACHER_BY_UUID_ENDPOINT,
  FETCH_TEACHER_BY_UUID_ENDPOINT,
  UPDATE_TEACHER_BY_UUID_ENDPOINT,
  SEARCH_SUBJECTs_ENDPOINT,
  FETCH_TEACHER_BY_EMAIL_ENDPOINT,
} from '../../../constants/url-paths';
import { AUTHORIZATION } from '../../../constants/common';
import { TeacherDto } from './teacher.dto';
import { TeachersSubjectInterface } from '../../components/teacher/teacher.component';

@Injectable({
  providedIn: 'root',
})
export class TeacherService {
  constructor(
    private readonly http: HttpClient,
    private readonly oauth2: OAuthService,
  ) {}

  fetchTeacherByUuid(uuid) {
    return this.http.get<TeacherDto>(
      FETCH_TEACHER_BY_UUID_ENDPOINT + '/' + uuid,
      { headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() } },
    );
  }

  createTeacher(teacher: TeacherDto) {
    return this.http.post(CREATE_TEACHER_ENDPOINT, teacher, {
      headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() },
    });
  }

  updateTeacher(uuid: string, teacher: TeacherDto) {
    return this.http.post<TeacherDto>(
      UPDATE_TEACHER_BY_UUID_ENDPOINT + '/' + uuid,
      teacher,
      { headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() } },
    );
  }

  deleteTeacher(uuid: string) {
    return this.http.post<TeacherDto>(
      DELETE_TEACHER_BY_UUID_ENDPOINT + '/' + uuid,
      { headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() } },
    );
  }

  getSubjects(value: string) {
    const params = new HttpParams()
      .set('value', value)
      .set('limit', '5')
      .set('offset', '0');
    return this.http.get<TeachersSubjectInterface[]>(SEARCH_SUBJECTs_ENDPOINT, {
      headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() },
      params,
    });
  }

  fetchTeacherByEmail() {
    const obj: any = this.oauth2.getIdentityClaims();
    return this.http.get<TeacherDto>(
      FETCH_TEACHER_BY_EMAIL_ENDPOINT + '/' + obj.email,
      { headers: { [AUTHORIZATION]: this.oauth2.authorizationHeader() } },
    );
  }
}
