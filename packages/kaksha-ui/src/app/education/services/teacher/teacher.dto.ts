import { TeachersSubjectInterface } from '../../components/teacher/teacher.component';

export class TeacherDto {
  email?: string;
  firstName?: string;
  middleName?: string;
  lastName?: string;
  joiningDate?: Date;
  leavingDate?: Date;
  name?: string;
  subjects?: TeachersSubjectInterface[];
}
