import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, from, of } from 'rxjs';
import { startWith, debounceTime, switchMap } from 'rxjs/operators';
import { NEW_ID } from '../../../constants/common';
import { CLOSE } from '../../../constants/messages';
import { StandardService } from '../../services/standard/standard.service';
import { TeacherService } from '../../services/teacher/teacher.service';
import { SubjectInterface } from '../standard/standard.component';
import { TeachersSubjectInterface } from './teacher.component';

@Component({
  selector: 'teacher-subject-dialog',
  templateUrl: './teacher-subject-dialog.html',
})
export class TeacherSubjectDialog implements OnInit {
  title: string = 'New Subject';
  subject: TeachersSubjectInterface;
  subjectFormControl = new FormControl('', [Validators.required]);
  subjectsList: Observable<TeachersSubjectInterface[]>;
  active: boolean = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(
    public dialogRef: MatDialogRef<TeacherSubjectDialog>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      subjectsList: TeachersSubjectInterface[];
      subject: TeachersSubjectInterface;
    },
    private snackBar: MatSnackBar,
    private teacherService: TeacherService,
    private standardService: StandardService,
  ) {
    if (this.data.subject.name !== NEW_ID) {
      this.title = 'Edit Subject';
      this.subject = this.data.subject;
      this.subjectFormControl.setValue(this.data.subject);
      if (!this.subject?.divisions?.length) {
        this.subject.divisions = [];
      }
    }
  }

  ngOnInit() {
    this.subjectsList = this.subjectFormControl.valueChanges.pipe(
      startWith(''),
      debounceTime(500),
      switchMap(value => {
        return this.teacherService.getSubjects(value);
      }),
    );
  }

  addDivision(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value.toUpperCase();

    if ((value || '').trim() && this.subject.divisions.indexOf(value) === -1) {
      this.subject.divisions.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  removeDivision(div: string): void {
    const index = this.subject.divisions.indexOf(div);

    if (index >= 0) {
      this.subject.divisions.splice(index, 1);
    }
  }

  saveSubject(subject: TeachersSubjectInterface) {
    if (!this.data.subjectsList.length) {
      this.dialogRef.close([subject]);
      return;
    }

    // Delete old subject from list if its replaced with new subject
    if (
      this.data.subject.uuid !== subject.uuid &&
      this.data.subject.uuid !== NEW_ID
    ) {
      this.data.subjectsList = this.data.subjectsList.filter(
        sub => sub.uuid !== this.data.subject.uuid,
      );
    }

    // get index if subject already exists
    const index = this.data.subjectsList.findIndex(
      sub => sub.uuid === subject.uuid,
    );

    if (index === -1) {
      this.dialogRef.close([...this.data.subjectsList, subject]);
      return;
    }

    const filteredSubjects = this.data.subjectsList.filter(sub => {
      if (sub.uuid === subject.uuid) {
        return Object.assign(sub, subject);
      }
    });

    this.dialogRef.close([...filteredSubjects]);
  }

  getSubjects() {}

  subjectSelected(subject: SubjectInterface) {
    this.standardService.getStandard(subject.standard).subscribe({
      next: res => {
        this.subject = { divisions: res.divisions, ...subject };
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error fetching standard');
      },
    });
  }

  deleteSubject(uuid: string) {
    const results = this.data.subjectsList.filter(sub => {
      if (sub.uuid !== uuid) {
        return sub;
      }
    });
    this.dialogRef.close(results);
  }

  getOptionText(option) {
    if (!option) return;
    return `${option.name} - ${option.standard}`;
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }
}
