import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

import {
  CLOSE,
  CREATE_ERROR,
  CREATE_SUCCESSFUL,
  DELETE_ERROR,
  UPDATE_ERROR,
  UPDATE_SUCCESSFUL,
} from '../../../constants/messages';
import { DURATION, NEW_ID } from '../../../constants/common';
import { TeacherDto } from '../../services/teacher/teacher.dto';
import { TeacherService } from '../../services/teacher/teacher.service';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { from, Observable, of } from 'rxjs';
import { debounceTime, startWith, switchMap } from 'rxjs/operators';
import { TeacherSubjectDialog } from './teacher-subject-dialog.component';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css'],
})
export class TeacherComponent implements OnInit {
  uuid: string;
  name: string;
  joiningDate: Date;
  leavingDate: Date;
  active: boolean = false;
  subjects: TeachersSubjectInterface[] = [];
  displayedColumns: string[] = ['standard', 'name', 'divisions'];
  teacherForm = new FormGroup({
    firstName: new FormControl(),
    middleName: new FormControl(),
    lastName: new FormControl(),
    email: new FormControl(),
    name: new FormControl(),
    joiningDate: new FormControl(),
    leavingDate: new FormControl(),
  });

  constructor(
    private readonly teacher: TeacherService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly dialog: MatDialog,
  ) {
    this.uuid = this.route.snapshot.params.uuid;
  }

  ngOnInit() {
    this.teacherForm.controls.joiningDate.disable();
    this.teacherForm.controls.leavingDate.disable();
    if (this.uuid !== NEW_ID) {
      this.fetchTeacher();
    }
  }

  updateTeacher() {
    this.teacher
      .updateTeacher(this.uuid, {
        email: this.teacherForm.controls.email.value,
        firstName: this.teacherForm.controls.firstName.value,
        middleName: this.teacherForm.controls.middleName.value,
        lastName: this.teacherForm.controls.lastName.value,
        joiningDate: this.teacherForm.controls.joiningDate.value,
        leavingDate: this.teacherForm.controls.leavingDate.value,
        name: this.teacherForm.controls.name.value,
        subjects: this.subjects,
      })
      .subscribe({
        next: success => {
          this.setResponse(success);
          this.getMessage(UPDATE_SUCCESSFUL);
        },
        error: error => {
          this.getMessage(UPDATE_ERROR);
        },
      });
  }

  createTeacher() {
    this.teacher
      .createTeacher({
        email: this.teacherForm.controls.email.value,
        firstName: this.teacherForm.controls.firstName.value,
        middleName: this.teacherForm.controls.middleName.value,
        lastName: this.teacherForm.controls.lastName.value,
        joiningDate: this.teacherForm.controls.joiningDate.value,
        leavingDate: this.teacherForm.controls.leavingDate.value,
        name: this.teacherForm.controls.name.value,
      })
      .subscribe({
        next: success => {
          this.getMessage(CREATE_SUCCESSFUL);
          this.routeToTeacherList();
        },
        error: error => {
          this.getMessage(error?.error?.message || CREATE_ERROR);
        },
      });
  }

  fetchTeacher() {
    this.teacher.fetchTeacherByUuid(this.uuid).subscribe({
      next: response => {
        this.setResponse(response);
      },
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }

  setResponse(response: TeacherDto) {
    this.name = response.name;
    this.joiningDate = response.joiningDate;
    this.leavingDate = response.leavingDate;
    this.teacherForm.controls.name.setValue(response.name);
    this.teacherForm.controls.email.setValue(response.email);
    if (response.email) {
      this.teacherForm.controls.email.disable();
    }
    this.teacherForm.controls.firstName.setValue(response.firstName);
    this.teacherForm.controls.middleName.setValue(response.middleName);
    this.teacherForm.controls.lastName.setValue(response.lastName);
    this.teacherForm.controls.joiningDate.setValue(response.joiningDate);
    if (response.joiningDate) {
      this.teacherForm.controls.joiningDate.disable();
    }
    this.teacherForm.controls.leavingDate.setValue(response.leavingDate);
    this.teacherForm.controls.leavingDate.disable();
    this.subjects = response.subjects || [];
    this.active = true;
  }

  deleteTeacher() {
    const dialogRef = this.dialog.open(DeleteDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.teacher.deleteTeacher(this.uuid).subscribe(
          next => {
            this.routeToTeacherList();
          },
          error => {
            this.snackBar.open(DELETE_ERROR, CLOSE, { duration: DURATION });
          },
        );
      }
    });
  }

  routeToTeacherList() {
    this.router.navigate(['list', 'teacher']);
  }

  openSubject(row?: TeachersSubjectInterface) {
    const dialogRef = this.dialog.open(TeacherSubjectDialog, {
      width: '250px',
      data: { subjectsList: this.subjects, subject: row || { name: NEW_ID } },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.getMessage('Subject saved');
      this.subjects = result;
      this.updateTeacher();
    });
  }

  updateDate(type: string, event: MatDatepickerInputEvent<Date>) {
    switch (type) {
      case 'joiningDate':
        this.joiningDate = event.value;
        break;
      case 'leavingDate':
        this.leavingDate = event.value;
        break;
    }
  }
}

export interface TeachersSubjectInterface {
  name: string;
  uuid: string;
  standard: string;
  divisions: string[];
  subjectState?: {
    [key: string]: {
      lastTopic: string;
      currentTopic: string;
      nextTopic: string;
      totalTopics: number;
      completedTopics: number;
    };
  };
}
