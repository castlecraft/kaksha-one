import { Component, OnInit } from '@angular/core';
import { OrganizationService } from '../../services/organization/organization.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css'],
})
export class OrganizationComponent implements OnInit {
  organizationName: string;
  email: string;
  constructor(private readonly organization: OrganizationService) {}

  ngOnInit(): void {
    this.organization.fetchOrganization().subscribe({
      next: res => {
        this.email = res.email;
        this.organizationName = res.name;
      },
      error: error => {},
    });
  }
}
