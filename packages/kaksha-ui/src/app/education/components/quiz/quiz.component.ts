import { Component, OnInit } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl, FormGroup } from '@angular/forms';
import { QuizService } from '../../services/quiz/quiz.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CLOSE } from '../../../constants/messages';
import { NEW_ID } from '../../../constants/common';
import { SubjectInterface } from '../subject/subject.component';
import { StandardInterface } from '../standard/standard.component';
import { StandardService } from '../../services/standard/standard.service';

export interface QuizInterface {
  uuid?: string;
  subject?: string;
  standard?: string;
  title?: string;
  type?: string;
  questions?: QuestionInterace;
}
export interface QuestionInterace {
  [key: string]: {
    question: string;
    options: {
      [key: string]: string;
    };
  };
}

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css'],
})
export class QuizComponent implements OnInit {
  standards: Array<StandardInterface> = [];
  subjects: Array<SubjectInterface> = [];
  quizForm: FormGroup;
  quiz: QuizInterface = {
    subject: '',
    standard: '',
    title: '',
    type: '',
    questions: {},
  };
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  options = {};

  constructor(
    private quizService: QuizService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private standardService: StandardService,
  ) {}

  ngOnInit(): void {
    this.createQuizForm();
    if (this.activatedRoute.snapshot.params.uuid !== NEW_ID) {
      this.quiz.uuid = this.activatedRoute.snapshot.params.uuid;
      this.getQuiz();
    }
    this.getStandardList();
    this.getSubjectList();
  }

  createQuizForm() {
    this.quizForm = new FormGroup({
      subject: new FormControl(this.quiz.subject),
      standard: new FormControl(this.quiz.standard),
      title: new FormControl(this.quiz.title),
      type: new FormControl(this.quiz.type),
    });
  }

  getQuiz() {
    this.quizService.getQuiz(this.quiz.uuid).subscribe({
      next: (quiz: any) => {
        this.quiz = quiz;
        Object.keys(this.quizForm?.controls || {}).forEach(control => {
          this.quizForm.controls[control].setValue(quiz[control]);
        });
      },
    });
  }

  saveQuiz() {
    this.quiz.type = this.quizForm.controls.type.value;
    this.quiz.title = this.quizForm.controls.title.value;
    this.quiz.subject = this.quizForm.controls.subject.value;
    this.quiz.standard = this.quizForm.controls.standard.value;
    this.quizService.saveQuiz(this.quiz).subscribe({
      next: quiz => {
        this.quiz = quiz;
        this.getMessage('Quiz Successfully created');
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving quiz');
      },
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }

  openQuestion(question) {
    const dialogRef = this.dialog.open(QuestionDialogueComponent, {
      width: '250px',
    });
    dialogRef.componentInstance.question = question.value.question;
    dialogRef.componentInstance.options = question.value.options;
    dialogRef.componentInstance.quiz = this.quiz;
    dialogRef.componentInstance.editQuestion = question;
  }

  newQuestionDialog(): void {
    const dialogRef = this.dialog.open(QuestionDialogueComponent, {
      width: '250px',
    });
    dialogRef.componentInstance.quiz = this.quiz;
  }

  getStandardList() {
    this.standardService.getStandardList().subscribe({
      next: (standardList: any) => {
        this.standards = standardList.docs;
      },
      error: err => {},
    });
  }

  getSubjectList() {
    this.standardService.getSubjectList().subscribe({
      next: (subjectList: any) => {
        this.subjects = subjectList.docs;
      },
      error: err => {},
    });
  }
}

@Component({
  selector: 'app-question-dialogue',
  templateUrl: './question-dialogue.component.html',
})
export class QuestionDialogueComponent implements OnInit {
  options: any = {};
  quiz: QuizInterface = {
    questions: {},
  };
  editQuestion: any = {};
  question: string = '';
  active: boolean = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit(): void {}

  constructor(
    public dialogRef: MatDialogRef<QuestionDialogueComponent>,
    private quizService: QuizService,
    private snackBar: MatSnackBar,
  ) {}

  deleteQuestion(): void {
    delete this.quiz.questions[this.editQuestion.key];
    const obj = this.quiz.questions;
    this.quiz.questions = {};
    Object.keys(obj).map(object => {
      this.question = obj[object].question;
      this.options = obj[object].options;
      this.addQuestion();
    });
    this.quizService.saveQuiz(this.quiz).subscribe({
      next: res => {
        this.getMessage('Question Removed Succesfully');
      },
      error: err => {},
    });
  }

  addOptions(event: any): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      const size = Object.keys(this.options).length;
      this.options[size] = value.trim();
    }
    if (input) {
      input.value = '';
    }
  }

  removeOptions(option: any): void {
    delete this.options[option.key];
    const obj = this.options;
    this.options = {};
    Object.keys(obj).map(object => {
      this.addOptions({ value: obj[object] });
    });
  }

  addQuestion() {
    if (this.editQuestion.value) {
      this.quiz.questions[this.editQuestion.key] = this.editQuestion.value;
      this.editQuestion = {};
    } else {
      const size = Object.keys(this.quiz.questions).length;
      this.quiz.questions[size] = {
        question: this.question,
        options: this.options,
      };
    }
    this.options = {};
    this.quizService.saveQuiz(this.quiz).subscribe({
      next: res => {
        this.getMessage('Question Added Successfully');
      },
      error: err => {},
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }

  disableButton() {
    return Object.entries(this.options).length && this.question.length;
  }
}
