import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../../common/services/storage/storage.service';
import { DEFAULT_MEET_OPTIONS, USER_INFO } from '../../../constants/storage';
import { MEET_EXTERNAL_API_URL } from '../../../constants/url-paths';

declare let JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-kaksha',
  templateUrl: './kaksha.component.html',
  styleUrls: ['./kaksha.component.css'],
})
export class KakshaComponent {
  user: UserInterface = {};
  options: any = DEFAULT_MEET_OPTIONS;

  meet: MeetControlsInterface = {
    active: false,
    loading$: false,
  };
  meetApi: any;

  constructor(
    private readonly storageService: StorageService,
    private readonly router: Router,
  ) {}

  startKaksha() {
    this.meet.active = true;
    this.meet.loading$ = true;
    this.storageService.getInfoAsync(USER_INFO).subscribe({
      next: (user: string) => {
        this.user = JSON.parse(user);

        this.options = {
          ...this.options,
          roomName: 'CEPL',
          parentNode: document.getElementById('#meet'),
        };

        this.options.userInfo.email = this.user.email;
        this.options.userInfo.displayName = this.user.name || this.user.email;
        this.meetApi = new JitsiMeetExternalAPI(
          MEET_EXTERNAL_API_URL,
          this.options,
        );
        this.meetApi.addEventListener(
          'videoConferenceJoined',
          this.videoConferenceJoined.bind(this),
        );
        this.meetApi.addEventListener('disconnect', this.disconnect.bind(this));
        this.meetApi.addEventListener(
          'videoConferenceLeft',
          this.videoConferenceLeft.bind(this),
        );
      },
    });
  }

  videoConferenceJoined(event) {
    this.meet.loading$ = false;
    this.meet.active = true;
  }

  videoConferenceLeft(event) {
    this.router.navigateByUrl('home');
  }

  onConnectionFailed(event) {
    this.meet.active = false;
  }

  disconnect(event) {
    this.meet.active = false;
    this.router.navigateByUrl('home');
  }
}

export interface UserInterface {
  email?: string;
  roles?: string[];
  name?: string;
}

export interface MeetControlsInterface {
  active?: boolean;
  loading$?: boolean;
}
