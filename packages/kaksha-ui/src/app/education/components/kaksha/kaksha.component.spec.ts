import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { StorageService } from '../../../common/services/storage/storage.service';
import { MaterialModule } from '../../../shared-imports/material/material.module';
import { KakshaComponent } from './kaksha.component';

describe('KakshaComponent', () => {
  let component: KakshaComponent;
  let fixture: ComponentFixture<KakshaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        RouterTestingModule,
      ],
      declarations: [KakshaComponent],
      providers: [
        {
          provide: StorageService,
          useValue: {},
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KakshaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
