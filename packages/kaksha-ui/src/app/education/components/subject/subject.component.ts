import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { NEW_ID } from '../../../constants/common';
import { CLOSE } from '../../../constants/messages';
import { StandardService } from '../../services/standard/standard.service';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css'],
})
export class SubjectComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  active: boolean = true;
  subject: SubjectInterface = {
    name: '',
    standard: '',
  };
  subjectForm = new FormGroup({
    name: new FormControl(this.subject.name),
    standard: new FormControl(this.subject.standard),
  });
  displayedColumns: string[] = ['name', 'createdBy'];
  topics: TopicsInterface[] = new Array(...[]);

  constructor(
    private activatedRoute: ActivatedRoute,
    private standardService: StandardService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.uuid !== NEW_ID) {
      this.subject.uuid = this.activatedRoute.snapshot.params.uuid;
      this.getSubject();
    }
  }

  getSubject() {
    this.standardService.getSubject(this.subject.uuid).subscribe({
      next: subject => {
        this.subject = subject;
        this.getTopics();
      },
      error: err => {},
    });
  }

  getTopics() {
    this.standardService
      .getTopics(this.subject.name, this.subject.standard)
      .subscribe({
        next: (topics: TopicsInterface[]) => {
          this.topics = topics;
        },
        error: err => {},
      });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }

  saveSubject() {
    this.subject.name = this.subjectForm.controls.name.value;
    this.subject.standard = this.subjectForm.controls.standard.value;
    this.standardService.saveSubject(this.subject).subscribe({
      next: (subject: SubjectInterface) => {
        this.subject = subject;
        this.getMessage('Subject updated.');
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving subject');
      },
    });
  }

  openTopic(row?) {
    const dialogRef = this.dialog.open(TopicDialog, {
      width: '250px',
      data: { subject: this.subject, topic: row || { name: NEW_ID } },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.getMessage('Topics saved');
      this.getTopics();
    });
  }
}

export interface SubjectInterface {
  uuid?: string;
  standard?: string;
  name?: string;
  createdBy?: string;
  createdOn?: Date;
  createdByEmail?: string;
}
export interface TopicsInterface {
  uuid?: string;
  subject?: string;
  standard?: string;
  name?: string;
  createdBy?: string;
  createdOn?: Date;
  createdByEmail?: string;
}

@Component({
  selector: 'topic-dialog',
  templateUrl: './topic-dialog.html',
})
export class TopicDialog {
  title: string = 'New Topic';
  topicName: string = '';

  constructor(
    public dialogRef: MatDialogRef<TopicDialog>,
    @Inject(MAT_DIALOG_DATA)
    public data: { subject: SubjectInterface; topic: TopicsInterface },
    public standardService: StandardService,
    private snackBar: MatSnackBar,
  ) {
    if (this.data.topic.name !== NEW_ID) {
      this.title = 'Edit Topic';
      this.topicName = this.data.topic.name;
    }
  }

  saveTopic() {
    this.data.topic.subject = this.data.subject.name;
    const topic = this.data.topic;
    topic.name = this.topicName;
    topic.standard = this.data.subject.standard;
    this.standardService.saveTopic(topic).subscribe({
      next: (subject: SubjectInterface) => {
        this.dialogRef.close(this.data.topic);
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving subject');
      },
    });
  }

  deleteTopic() {
    this.topicName = this.data.topic.name;
    this.standardService.deleteTopic(this.data.topic).subscribe({
      next: (subject: SubjectInterface) => {
        this.dialogRef.close(this.data.topic);
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving subject');
      },
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }
}
