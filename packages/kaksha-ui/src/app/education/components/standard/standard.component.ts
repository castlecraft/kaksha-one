import { Component, Inject, OnInit } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NEW_ID } from '../../../constants/common';
import { StandardService } from '../../services/standard/standard.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CLOSE } from '../../../constants/messages';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

@Component({
  selector: 'app-standard',
  templateUrl: './standard.component.html',
  styleUrls: ['./standard.component.css'],
})
export class StandardComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  active: boolean = true;
  standard: StandardInterface = {
    name: '',
    divisions: ['A'],
  };
  standardForm = new FormGroup({
    name: new FormControl(this.standard.name),
    divisions: new FormControl(this.standard.divisions),
  });
  displayedColumns: string[] = ['name', 'createdBy'];
  subjects: SubjectInterface[] = new Array(...[]);

  constructor(
    private activatedRoute: ActivatedRoute,
    private standardService: StandardService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.params.uuid !== NEW_ID) {
      this.standard.uuid = this.activatedRoute.snapshot.params.uuid;
      this.getStandard();
    }
  }

  addDivision(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value.toUpperCase();

    if ((value || '').trim() && this.standard.divisions.indexOf(value) === -1) {
      this.standard.divisions.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  removeDivision(div: string): void {
    const index = this.standard.divisions.indexOf(div);

    if (index >= 0) {
      this.standard.divisions.splice(index, 1);
    }
  }

  getStandard() {
    this.standardService.getStandard(this.standard.uuid).subscribe({
      next: standard => {
        this.standard = standard;
        this.getSubjects();
      },
      error: err => {},
    });
  }

  getSubjects() {
    this.standardService.getSubjects(this.standard.name).subscribe({
      next: (subjects: SubjectInterface[]) => {
        this.subjects = subjects;
      },
      error: err => {},
    });
  }

  saveStandard() {
    this.standard.name = this.standardForm.controls.name.value;
    this.standardService.saveStandard(this.standard).subscribe({
      next: (standard: StandardInterface) => {
        this.standard = standard;
        this.getMessage('Standard updated.');
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving standard');
      },
    });
  }

  openSubject(row?) {
    const dialogRef = this.dialog.open(SubjectDialog, {
      width: '250px',
      data: { standard: this.standard, subject: row || { name: NEW_ID } },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.getMessage('Subject saved');
      this.getSubjects();
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }
}

export interface StandardInterface {
  name?: string;
  divisions?: string[];
  uuid?: string;
}

export interface SubjectInterface {
  uuid: string;

  standard: string;

  name: string;

  createdBy: string;

  createdOn: Date;

  createdByEmail: string;
}

@Component({
  selector: 'subject-dialog',
  templateUrl: './subject-dialog.html',
})
export class SubjectDialog {
  title: string = 'New Subject';
  subjectName: string = '';

  constructor(
    public dialogRef: MatDialogRef<SubjectDialog>,
    @Inject(MAT_DIALOG_DATA)
    public data: { standard: StandardInterface; subject: SubjectInterface },
    public standardService: StandardService,
    private snackBar: MatSnackBar,
  ) {
    if (this.data.subject.name !== NEW_ID) {
      this.title = 'Edit Subject';
      this.subjectName = this.data.subject.name;
    }
  }

  saveSubject() {
    this.data.subject.standard = this.data.standard.name;
    const subject = this.data.subject;
    subject.name = this.subjectName;
    this.standardService.saveSubject(subject).subscribe({
      next: (standard: StandardInterface) => {
        this.dialogRef.close(this.data.subject);
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving standard');
      },
    });
  }

  deleteSubject() {
    this.standardService.deleteSubject(this.data.subject).subscribe({
      next: (standard: StandardInterface) => {
        this.dialogRef.close(this.data.subject);
      },
      error: err => {
        this.getMessage(err?.error?.message || 'Error in saving standard');
      },
    });
  }

  getMessage(message) {
    this.snackBar.open(message, CLOSE, { duration: 2500 });
  }
}
