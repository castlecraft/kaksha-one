import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../../shared-imports/material/material.module';
import { StandardService } from '../../services/standard/standard.service';

import { StandardComponent } from './standard.component';

describe('StandardComponent', () => {
  let component: StandardComponent;
  let fixture: ComponentFixture<StandardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StandardComponent],
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: FormBuilder,
          useValue: {
            group: (...args) => {},
          },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: of({ id: 'new' }),
            },
          },
        },
        {
          provide: StandardService,
          useValue: {
            getStandard: (...args) => of({}),
            getSubjects: (...args) => of([]),
            saveStandard: (...args) => of({}),
            saveSubject: (...args) => of({}),
          },
        },
        {
          provide: MatSnackBar,
          useValue: {},
        },
        {
          provide: MatDialog,
          useValue: {},
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
