export const LOGOUT_URL = '/auth/logout';
export const ADD_BUTTON_ROUTES = [
  'student',
  'teacher',
  'quiz',
  'subject',
  'standard',
];
export const FETCH_ORG_ENDPOINT = '/api/organization/v1/fetch';
export const CREATE_QUIZ_ENDPOINT = '/quiz/v1/create';
export const UPDATE_QUIZ_ENDPOINT = '/quiz/v1/update';
export const GET_QUIZ_ENDPOINT = '/quiz/v1/get/';
export const GET_STANDARD_ENDPOINT = '/standard/v1/get/';
export const GET_SUBJECT_ENDPOINT = '/subject/v1/get/';
export const GET_ALL_SUBJECTS_ENDPOINT = '/subject/v1/get_all/';
export const GET_SUBJECTS_LIST_ENDPOINT = '/subject/v1/list';
export const GET_STANDARD_LIST_ENDPOINT = '/standard/v1/list';
export const CREATE_TOPIC_ENDPOINT = '/topic/v1/create';
export const DELETE_TOPIC_ENDPOINT = '/topic/v1/remove';
export const UPDATE_TOPIC_ENDPOINT = '/topic/v1/update';
export const GET_ALL_TOPIC_ENDPOINT = '/topic/v1/get_all';
export const UPDATE_STANDARD_ENDPOINT = '/standard/v1/update';
export const CREATE_STANDARD_ENDPOINT = '/standard/v1/create';
export const UPDATE_SUBJECT_ENDPOINT = '/subject/v1/update';
export const CREATE_SUBJECT_ENDPOINT = '/subject/v1/create';
export const DELETE_SUBJECT_ENDPOINT = '/subject/v1/remove';
export const FETCH_TEACHER_BY_EMAIL_ENDPOINT =
  '/api/teacher/v1/fetch_teacher_by_email';
export const FETCH_TEACHER_BY_UUID_ENDPOINT =
  '/api/teacher/v1/fetch_teacher_by_uuid';
export const UPDATE_TEACHER_BY_UUID_ENDPOINT = '/api/teacher/v1/update_teacher';
export const CREATE_TEACHER_ENDPOINT = '/api/teacher/v1/add_teacher';
export const DELETE_TEACHER_BY_UUID_ENDPOINT = '/api/teacher/v1/delete';
export const SEARCH_SUBJECTs_ENDPOINT = '/api/subject/v1/search';
export const MEET_EXTERNAL_API_URL = 'meet.castlecraft.in';
