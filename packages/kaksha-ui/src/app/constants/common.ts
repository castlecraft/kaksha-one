export const NEW_ID: string = 'new';
export const MODELS = [{ route: 'student' }, { route: 'teacher' }];
export const DURATION = 5000;
export const UNDO_DURATION = 10000;
export const SCOPE = 'openid email roles profile';
export const AUTHORIZATION = 'Authorization';
export const API_PREFIX = 'api';
