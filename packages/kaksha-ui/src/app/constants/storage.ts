export const CLIENT_ID = 'client_id';
export const REDIRECT_URI = 'redirect_uri';
export const SILENT_REFRESH_REDIRECT_URI = 'silent_refresh_redirect_uri';
export const LOGIN_URL = 'login_url';
export const ISSUER_URL = 'issuer_url';
export const APP_URL = 'app_url';
export const USER_UUID = 'user_uuid';
export const SERVICES = 'services';
export const STORAGE = 'storage';
export const ACCESS_TOKEN = 'access_token';
export const COMMUNICATION = 'communication';
export const COMMUNICATION_SERVER = 'communication-server';
export const INFRASTRUCTURE_CONSOLE = 'infrastructure-console';
export const IDENTITY_PROVIDER = 'identity-provider';
export const COMMUNICATION_SERVER_URL = 'communication-server-url';
export const LIST = 'list';
export const API_PREFIX = 'api';
export const USER_INFO = 'user_info';

export const DEFAULT_MEET_OPTIONS = {
  width: '1080px',
  height: '555px',
  configOverwrite: {
    startWithAudioMuted: true,
    startWithVideoMuted: true,
    prejoinPageEnabled: true,
    SUPPORT_URL: 'https://castlecraft.in',
    DEFAULT_LOGO_URL: 'https://www.castlecraft.in/img/cepl/logo.png',
    DEFAULT_WELCOME_PAGE_LOGO_URL:
      'https://www.castlecraft.in/img/cepl/logo.png',
  },
  interfaceConfigOverwrite: {
    MOBILE_APP_PROMO: false,
  },
  userInfo: {
    email: 'kaksha@castlecraft.com',
    displayName: 'Kaksha User',
  },
};
