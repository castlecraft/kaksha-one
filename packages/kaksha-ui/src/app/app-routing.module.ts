import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { OrganizationComponent } from './education/components/organization/organization.component';
import { QuizComponent } from './education/components/quiz/quiz.component';
import { StandardComponent } from './education/components/standard/standard.component';
import { TeacherComponent } from './education/components/teacher/teacher.component';
import { SubjectComponent } from './education/components/subject/subject.component';
import { KakshaComponent } from './education/components/kaksha/kaksha.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'list/:model',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  { path: 'organization', component: OrganizationComponent },
  { path: 'quiz', component: QuizComponent },

  { path: 'quiz/:uuid', component: QuizComponent },
  { path: 'kaksha', component: KakshaComponent },
  { path: 'kaksha/:uuid', component: KakshaComponent },

  { path: 'subject/:uuid', component: SubjectComponent },
  { path: 'standard/:uuid', component: StandardComponent },
  { path: 'teacher/:uuid', component: TeacherComponent },

  // IMPORTANT: Keep this at the end of array
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
