import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Request } from 'express';

import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RoleConstants } from '../../../constants/app-strings';
import { SetupOrganizationCommand } from '../../commands/organization/setup-organization/setup-organization.command';
import { FetchOrganizationQuery } from '../../queries/fetch-organization/fetch-organization.query';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { TokenPatchGuard } from '../../../auth/guards/token-patch.guard';
import { OrganizationDto } from '../../entities/organization/organization.dto';
import { UpdateOrganizationCommand } from '../../commands/organization/update-organization/update-organization.command';

@Controller('organization')
export class OrganizationController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get('v1/fetch')
  @UseGuards(TokenPatchGuard)
  async fetch(@Req() req: Request & { token: TokenCache }) {
    return await this.queryBus.execute(new FetchOrganizationQuery(req.token));
  }

  @Post('v1/setup')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(RoleConstants.Administrator)
  @UseGuards(TokenGuard, RoleGuard)
  async setup(
    @Body() payload: OrganizationDto,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.commandBus.execute(
      new SetupOrganizationCommand(payload, req.token),
    );
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(RoleConstants.Administrator)
  @UseGuards(TokenGuard, RoleGuard)
  async update(
    @Body() payload: OrganizationDto,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.commandBus.execute(
      new UpdateOrganizationCommand(payload, req.token),
    );
  }
}
