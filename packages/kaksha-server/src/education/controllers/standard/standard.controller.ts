import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { UpdateStandardCommand } from '../../commands/standard/update-standard/update-standard.command';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ListQueryDto } from '../../../common/list-query.dto';
import { StandardAggregateService } from '../../aggregates/standard/standard-aggregate.service';
import { AddStandardCommand } from '../../commands/standard/add-standard/add-standard.command';
import { RemoveStandardCommand } from '../../commands/standard/remove-standard/remove-standard.command';
import {
  StandardDto,
  UpdateStandardDto,
} from '../../entities/standard/standard.dto';

@Controller('standard')
export class StandardController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly aggregate: StandardAggregateService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() standardPayload: StandardDto, @Req() req) {
    return await this.commandBus.execute(
      new AddStandardCommand(standardPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveStandardCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateStandard(@Body() updatePayload: UpdateStandardDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateStandardCommand(updatePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getStandard(@Param('uuid') uuid, @Req() req) {
    return await this.aggregate.getStandard(uuid);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  istStandard(@Query() query: ListQueryDto, @Req() req) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return this.aggregate.listStandard(offset, limit, sort, filter);
  }
}
