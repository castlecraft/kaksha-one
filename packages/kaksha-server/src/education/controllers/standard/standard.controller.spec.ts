import { CommandBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { StandardAggregateService } from '../../aggregates/standard/standard-aggregate.service';
import { StandardController } from './standard.controller';

describe('StandardController', () => {
  let controller: StandardController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StandardController],
      providers: [
        {
          provide: StandardAggregateService,
          useValue: {},
        },
        {
          provide: CommandBus,
          useValue: {},
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();

    controller = module.get<StandardController>(StandardController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
