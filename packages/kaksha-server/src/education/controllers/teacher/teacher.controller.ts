import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Request } from 'express';

import { AddTeacherCommand } from '../../commands/teacher/add-teacher/add-teacher.command';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { TeacherDto } from '../../entities/teacher/teacher.dto';
import { UpdateTeacherDto } from '../../entities/teacher/update-teacher.dto';
import { ListQueryDto } from '../../../common/list-query.dto';
import { ListTeachersQuery } from '../../queries/list-teachers/list-teachers.query';
import { UpdateTeacherCommand } from '../../commands/teacher/update-teacher/update-teacher.command';
import { FetchTeacherQuery } from '../../queries/fetch-teacher/fetch-teacher.query';
import { Teacher } from '../../entities/teacher/teacher.entity';
import { DeleteTeacherCommand } from '../../commands/teacher/delete-teacher/delete-teacher.command';

@Controller('teacher')
export class TeacherController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/add_teacher')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addTeacher(
    @Body() payload: TeacherDto,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.commandBus.execute(
      new AddTeacherCommand(payload, req.token),
    );
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async list(
    @Query() query: ListQueryDto,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.queryBus.execute(new ListTeachersQuery(query, req.token));
  }

  @Post('v1/update_teacher/:uuid')
  @UseGuards(TokenGuard)
  async updateTeacher(
    @Param('uuid') uuid: string,
    @Body() payload: UpdateTeacherDto,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.commandBus.execute(
      new UpdateTeacherCommand(uuid, payload, req.token),
    );
  }

  @Get('v1/fetch_teacher_by_uuid/:uuid')
  @UseGuards(TokenGuard)
  fetchTeacherByUuid(
    @Param('uuid') uuid: string,
    @Req() req: Request & { token: TokenCache },
  ) {
    return this.queryBus.execute(
      new FetchTeacherQuery({ uuid } as Teacher, req.token),
    );
  }

  @Get('v1/fetch_teacher_by_email/:email')
  @UseGuards(TokenGuard)
  fetchTeacherByEmail(
    @Param('email') email: string,
    @Req() req: Request & { token: TokenCache },
  ) {
    return this.queryBus.execute(
      new FetchTeacherQuery({ email } as Teacher, req.token),
    );
  }

  @Post('v1/delete/:uuid')
  @UseGuards(TokenGuard)
  async deleteTeacher(
    @Param('uuid') uuid: string,
    @Req() req: Request & { token: TokenCache },
  ) {
    return await this.commandBus.execute(
      new DeleteTeacherCommand(uuid, req.token),
    );
  }
}
