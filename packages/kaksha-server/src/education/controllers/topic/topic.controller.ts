import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { GetAllTopicsDto, ListQueryDto } from '../../../common/list-query.dto';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';
import { AddTopicCommand } from '../../commands/topic/add-topic/add-topic.command';
import { RemoveTopicCommand } from '../../commands/topic/remove-topic/remove-topic.command';
import { UpdateTopicCommand } from '../../commands/topic/update-topic/update-topic.command';
import { TopicDto, UpdateTopicDto } from '../../entities/topic/topic.dto';

@Controller('topic')
export class TopicController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly aggregate: TopicAggregateService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() topicPayload: TopicDto, @Req() req) {
    return await this.commandBus.execute(
      new AddTopicCommand(topicPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveTopicCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateTopic(@Body() updatePayload: UpdateTopicDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateTopicCommand(updatePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getTopic(@Param('uuid') uuid, @Req() req) {
    return await this.aggregate.getTopic(uuid);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  listTopic(@Query() query: ListQueryDto, @Req() req) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return this.aggregate.listTopic(offset, limit, sort, filter);
  }

  @Get('v1/get_all')
  @UseGuards(TokenGuard)
  async getAllLinkedTopics(@Query() query: GetAllTopicsDto) {
    const { standardName, subjectName } = query;
    if (!subjectName || !standardName)
      throw new BadRequestException('Subject Name is mandatory');
    return await this.aggregate.getAllTopics(subjectName, standardName);
  }
}
