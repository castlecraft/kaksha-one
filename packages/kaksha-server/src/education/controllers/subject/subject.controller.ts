import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { ListQueryDto } from '../../../common/list-query.dto';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { SubjectAggregateService } from '../../aggregates/subject/subject-aggregate.service';
import { AddSubjectCommand } from '../../commands/subject/add-subject/add-subject.command';
import { RemoveSubjectCommand } from '../../commands/subject/remove-subject/remove-subject.command';
import { UpdateSubjectCommand } from '../../commands/subject/update-subject/update-subject.command';
import {
  SubjectDto,
  UpdateSubjectDto,
} from '../../entities/subject/subject.dto';

@Controller('subject')
export class SubjectController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly aggregate: SubjectAggregateService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() subjectPayload: SubjectDto, @Req() req) {
    return await this.commandBus.execute(
      new AddSubjectCommand(subjectPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveSubjectCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateSubject(@Body() updatePayload: UpdateSubjectDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateSubjectCommand(updatePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getSubject(@Param('uuid') uuid, @Req() req) {
    return await this.aggregate.getSubject(uuid);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  listSubject(@Query() query: ListQueryDto, @Req() req) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return this.aggregate.listSubject(offset, limit, sort, filter);
  }

  @Get('v1/get_all/:standardName')
  @UseGuards(TokenGuard)
  async getAllLinkedSubjects(@Param('standardName') standardName: string) {
    if (!standardName)
      throw new BadRequestException('Standard Name is mandatory');
    return await this.aggregate.getAllSubjects(standardName);
  }

  @Get('v1/search')
  @UseGuards(TokenGuard)
  async search(@Query() query: any) {
    const { offset, limit, filter_query, value } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return await this.aggregate.search(value, offset, limit, filter);
  }
}
