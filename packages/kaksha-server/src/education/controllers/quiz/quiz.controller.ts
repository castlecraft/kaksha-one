import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Param,
  Body,
  Req,
  Query,
  Get,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { AddQuizCommand } from '../../commands/quiz/add-quiz/add-quiz.command';
import { RemoveQuizCommand } from '../../commands/quiz/remove-quiz/remove-quiz.command';
import { UpdateQuizCommand } from '../../commands/quiz/update-quiz/update-standard.command';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ListQueryDto } from '../../../common/list-query.dto';
import { QuizAggregateService } from '../../aggregates/quiz-aggregate/quiz-aggregate.service';
import { UpdateQuizDto, QuizDto } from '../../entities/quiz/quiz.dto';

@Controller('quiz')
export class QuizController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly aggregate: QuizAggregateService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() quizPayload: QuizDto, @Req() req) {
    return await this.commandBus.execute(new AddQuizCommand(quizPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  async remove(@Param('uuid') uuid) {
    return await this.commandBus.execute(new RemoveQuizCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateQuiz(@Body() updatePayload: UpdateQuizDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateQuizCommand(updatePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getQuiz(@Param('uuid') uuid, @Req() req) {
    return await this.aggregate.getQuiz(uuid);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  listQuiz(@Query() query: ListQueryDto, @Req() req) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return this.aggregate.listQuiz(offset, limit, sort, filter);
  }
}
