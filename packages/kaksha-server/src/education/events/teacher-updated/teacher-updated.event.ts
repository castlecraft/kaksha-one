import { IEvent } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { Teacher } from '../../entities/teacher/teacher.entity';

export class TeacherUpdatedEvent implements IEvent {
  constructor(
    public readonly teacher: Teacher,
    public readonly token: TokenCache,
  ) {}
}
