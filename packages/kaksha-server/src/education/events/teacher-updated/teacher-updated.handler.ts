import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { TeacherUpdatedEvent } from './teacher-updated.event';

@EventsHandler(TeacherUpdatedEvent)
export class TeacherUpdatedHandler implements IEventHandler {
  constructor(private readonly teacher: TeacherService) {}

  handle(event: TeacherUpdatedEvent) {
    from(
      this.teacher.updateOne(
        { uuid: event.teacher.uuid },
        { $set: event.teacher },
      ),
    ).subscribe({
      error: error => {
        error;
      },
      next: success => {
        success;
      },
    });
  }
}
