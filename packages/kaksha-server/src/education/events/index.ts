import { OrganizationSetupCompletedHandler } from './organization-setup-completed/organization-setup-completed.handler';
import { OrganizationUpdatedHandler } from './organization-updated/organization-updated.handler';
import { TeacherAddedHandler } from './teacher-added/teacher-added.handler';
import { TeacherDeletedHandler } from './teacher-deleted/teacher-deleted.handler';
import { TeacherUpdatedHandler } from './teacher-updated/teacher-updated.handler';

export const EducationEventHandlers = [
  OrganizationSetupCompletedHandler,
  OrganizationUpdatedHandler,
  TeacherAddedHandler,
  TeacherUpdatedHandler,
  TeacherDeletedHandler,
];
