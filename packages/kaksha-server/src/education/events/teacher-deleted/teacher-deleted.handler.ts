import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { TeacherDeletedEvent } from './teacher-deleted.event';

@EventsHandler(TeacherDeletedEvent)
export class TeacherDeletedHandler implements IEventHandler {
  constructor(private readonly teacher: TeacherService) {}

  handle(event: TeacherDeletedEvent) {
    from(this.teacher.deleteOne({ uuid: event.teacher.uuid })).subscribe({
      error: error => {},
      next: success => {},
    });
  }
}
