import { BadRequestException, HttpService } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AxiosError } from 'axios';
import { from, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { INVALID_USER } from '../../../constants/messages';

import {
  APPLICATION_JSON_CONTENT_TYPE,
  CONTENT_TYPE,
} from '../../../constants/app-strings';
import {
  USER_SIGN_UP_ENDPOINT,
  VERIFY_USER_ENDPOINT,
} from '../../../constants/url-endpoints';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { TeacherAddedEvent } from './teacher-added.event';

@EventsHandler(TeacherAddedEvent)
export class TeacherAddedHandler implements IEventHandler {
  constructor(
    private readonly teacher: TeacherService,
    private readonly http: HttpService,
    private readonly settings: SettingsService,
  ) {}

  handle(event: TeacherAddedEvent) {
    from(this.teacher.insertOne(event.teacher))
      .pipe(
        switchMap(teacher => {
          return this.settings.find();
        }),
        switchMap(settings => {
          return this.http.post(
            settings.authServerURL + VERIFY_USER_ENDPOINT,
            { username: event.teacher.email },
            {
              headers: {
                [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
              },
            },
          );
        }),
        catchError((error: AxiosError) => {
          if (
            error &&
            error.response &&
            error.response.status &&
            error.response.status === 400 &&
            error.response.data &&
            error.response.data.message &&
            error.response.data.message === INVALID_USER
          ) {
            return this.settings.find().pipe(
              switchMap(settings => {
                return this.http
                  .post(settings.authServerURL + USER_SIGN_UP_ENDPOINT, {
                    name: event.teacher.name,
                    email: event.teacher.email,
                  })
                  .pipe(map(res => res.data));
              }),
            );
          }
          return throwError(new BadRequestException(error));
        }),
      )
      .subscribe({
        error: error => {},
        next: success => {},
      });
  }
}
