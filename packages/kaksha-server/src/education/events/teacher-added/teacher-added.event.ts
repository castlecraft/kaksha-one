import { IEvent } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { Teacher } from '../../entities/teacher/teacher.entity';

export class TeacherAddedEvent implements IEvent {
  constructor(
    public readonly teacher: Teacher,
    public readonly token: TokenCache,
  ) {}
}
