import { IEvent } from '@nestjs/cqrs';
import { Organization } from '../../entities/organization/organization.entity';

export class OrganizationUpdatedEvent implements IEvent {
  constructor(public readonly organization: Organization) {}
}
