import { Injectable } from '@nestjs/common';
import { OrganizationService } from '../../entities/organization/organization.service';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { IPolicy } from '../../../common/policy.interface';

@Injectable()
export class ValidateOrganizationAdminService implements IPolicy {
  constructor(private readonly organization: OrganizationService) {}
  async validate(token: TokenCache) {
    const organizations = await this.organization.find();
    if (organizations.length > 0) {
      const organization = organizations[0];
      if (organization.email === token.email) {
        return true;
      }
    }
    return false;
  }
}
