import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationService } from '../../entities/organization/organization.service';
import { ValidateOrganizationAdminService } from './validate-organization-admin.service';

describe('ValidateOrganizationAdminService', () => {
  let service: ValidateOrganizationAdminService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ValidateOrganizationAdminService,
        { provide: OrganizationService, useValue: {} },
      ],
    }).compile();

    service = module.get<ValidateOrganizationAdminService>(
      ValidateOrganizationAdminService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
