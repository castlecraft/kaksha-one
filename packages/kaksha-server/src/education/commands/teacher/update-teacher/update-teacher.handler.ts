import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TeacherAggregateService } from '../../../aggregates/teacher-aggregate/teacher-aggregate.service';
import { UpdateTeacherCommand } from './update-teacher.command';

@CommandHandler(UpdateTeacherCommand)
export class UpdateTeacherHandler
  implements ICommandHandler<UpdateTeacherCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TeacherAggregateService,
  ) {}

  async execute(command: UpdateTeacherCommand) {
    const { uuid, payload, token } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.updateTeacher(uuid, payload, token);
    aggregate.commit();
    return response;
  }
}
