import { ICommand } from '@nestjs/cqrs';
import { UpdateTeacherDto } from '../../../entities/teacher/update-teacher.dto';
import { TokenCache } from '../../../../auth/entities/token-cache/token-cache.entity';

export class UpdateTeacherCommand implements ICommand {
  constructor(
    public readonly uuid: string,
    public readonly payload: UpdateTeacherDto,
    public readonly token: TokenCache,
  ) {}
}
