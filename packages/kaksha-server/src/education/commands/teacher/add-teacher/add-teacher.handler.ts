import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TeacherAggregateService } from '../../../aggregates/teacher-aggregate/teacher-aggregate.service';
import { AddTeacherCommand } from './add-teacher.command';

@CommandHandler(AddTeacherCommand)
export class AddTeacherHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TeacherAggregateService,
  ) {}

  async execute(command: AddTeacherCommand) {
    const { payload, token } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.addTeacher(payload, token);
    aggregate.commit();
    return response;
  }
}
