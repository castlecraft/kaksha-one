import { ICommand } from '@nestjs/cqrs';
import { TeacherDto } from '../../../entities/teacher/teacher.dto';
import { TokenCache } from '../../../../auth/entities/token-cache/token-cache.entity';

export class AddTeacherCommand implements ICommand {
  constructor(
    public readonly payload: TeacherDto,
    public readonly token: TokenCache,
  ) {}
}
