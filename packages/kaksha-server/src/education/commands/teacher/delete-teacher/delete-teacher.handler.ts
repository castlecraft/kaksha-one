import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TeacherAggregateService } from '../../../aggregates/teacher-aggregate/teacher-aggregate.service';
import { DeleteTeacherCommand } from './delete-teacher.command';

@CommandHandler(DeleteTeacherCommand)
export class DeleteTeacherHandler
  implements ICommandHandler<DeleteTeacherCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TeacherAggregateService,
  ) {}

  async execute(command: DeleteTeacherCommand) {
    const { uuid, token } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.deleteTeacher(uuid, token);
    aggregate.commit();
    return response;
  }
}
