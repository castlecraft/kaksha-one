import { ICommand } from '@nestjs/cqrs';
import { TokenCache } from '../../../../auth/entities/token-cache/token-cache.entity';

export class DeleteTeacherCommand implements ICommand {
  constructor(
    public readonly uuid: string,
    public readonly token: TokenCache,
  ) {}
}
