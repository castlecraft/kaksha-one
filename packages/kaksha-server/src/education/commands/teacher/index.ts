import { AddTeacherHandler } from './add-teacher/add-teacher.handler';
import { DeleteTeacherHandler } from './delete-teacher/delete-teacher.handler';
import { UpdateTeacherHandler } from './update-teacher/update-teacher.handler';

export const TeacherCommandHandlers = [
  AddTeacherHandler,
  DeleteTeacherHandler,
  UpdateTeacherHandler,
];
