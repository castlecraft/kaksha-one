import { SetupOrganizationHandler } from './setup-organization/setup-organization.handler';
import { UpdateOrganizationHandler } from './update-organization/update-organization.handler';

export const OrganizationCommandHandlers = [
  SetupOrganizationHandler,
  UpdateOrganizationHandler,
];
