import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { OrganizationAggregateService } from '../../../aggregates/organization-aggregate/organization-aggregate.service';
import { SetupOrganizationCommand } from './setup-organization.command';

@CommandHandler(SetupOrganizationCommand)
export class SetupOrganizationHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: OrganizationAggregateService,
  ) {}

  async execute(command: SetupOrganizationCommand) {
    const { payload, token } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.setupOrganization(payload, token);
    aggregate.commit();
    return response;
  }
}
