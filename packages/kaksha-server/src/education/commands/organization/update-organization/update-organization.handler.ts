import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { OrganizationAggregateService } from '../../../aggregates/organization-aggregate/organization-aggregate.service';
import { UpdateOrganizationCommand } from './update-organization.command';

@CommandHandler(UpdateOrganizationCommand)
export class UpdateOrganizationHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: OrganizationAggregateService,
  ) {}

  async execute(command: UpdateOrganizationCommand) {
    const { payload, token } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const response = await aggregate.updateOrganization(payload, token);
    aggregate.commit();
    return response;
  }
}
