import { ICommand } from '@nestjs/cqrs';
import { TokenCache } from '../../../../auth/entities/token-cache/token-cache.entity';
import { OrganizationDto } from '../../../entities/organization/organization.dto';

export class UpdateOrganizationCommand implements ICommand {
  constructor(
    public readonly payload: OrganizationDto,
    public readonly token: TokenCache,
  ) {}
}
