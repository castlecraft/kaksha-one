import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TopicAggregateService } from '../../../aggregates/topic-aggregate/topic-aggregate.service';
import { AddTopicCommand } from './add-topic.command';

@CommandHandler(AddTopicCommand)
export class AddTopicHandler implements ICommandHandler<AddTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}
  async execute(command: AddTopicCommand) {
    const { topicPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const topic = await aggregate.addTopic(topicPayload, clientHttpRequest);
    aggregate.commit();
    return topic;
  }
}
