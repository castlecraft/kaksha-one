import { ICommand } from '@nestjs/cqrs';
import { TopicDto } from '../../../entities/topic/topic.dto';

export class AddTopicCommand implements ICommand {
  constructor(
    public topicPayload: TopicDto,
    public readonly clientHttpRequest: any,
  ) {}
}
