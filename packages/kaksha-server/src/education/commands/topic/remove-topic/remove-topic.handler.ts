import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TopicAggregateService } from '../../../aggregates/topic-aggregate/topic-aggregate.service';
import { RemoveTopicCommand } from './remove-topic.command';

@CommandHandler(RemoveTopicCommand)
export class RemoveTopicHandler implements ICommandHandler<RemoveTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}

  async execute(command: RemoveTopicCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeTopic(uuid);
    aggregate.commit();
    return;
  }
}
