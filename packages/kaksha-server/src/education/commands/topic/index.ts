import { AddTopicHandler } from './add-topic/add-topic.handler';
import { RemoveTopicHandler } from './remove-topic/remove-topic.handler';
import { UpdateTopicHandler } from './update-topic/update-topic.handler';

export const TopicCommandHandlers = [
  AddTopicHandler,
  RemoveTopicHandler,
  UpdateTopicHandler,
];
