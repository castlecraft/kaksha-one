import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { TopicAggregateService } from '../../../aggregates/topic-aggregate/topic-aggregate.service';
import { UpdateTopicCommand } from './update-topic.command';

@CommandHandler(UpdateTopicCommand)
export class UpdateTopicHandler implements ICommandHandler<UpdateTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}

  async execute(command: UpdateTopicCommand) {
    const { updatePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateTopic(updatePayload, clientHttpRequest);
    aggregate.commit();
    return updatePayload;
  }
}
