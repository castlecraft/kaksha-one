import { ICommand } from '@nestjs/cqrs';
import { UpdateTopicDto } from '../../../entities/topic/topic.dto';

export class UpdateTopicCommand implements ICommand {
  constructor(
    public readonly updatePayload: UpdateTopicDto,
    public readonly clientHttpRequest: any,
  ) {}
}
