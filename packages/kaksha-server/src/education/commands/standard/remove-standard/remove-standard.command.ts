import { ICommand } from '@nestjs/cqrs';

export class RemoveStandardCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
