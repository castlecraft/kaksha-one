import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { RemoveStandardCommand } from './remove-standard.command';
import { StandardAggregateService } from '../../../aggregates/standard/standard-aggregate.service';

@CommandHandler(RemoveStandardCommand)
export class RemoveStandardHandler
  implements ICommandHandler<RemoveStandardCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StandardAggregateService,
  ) {}

  async execute(command: RemoveStandardCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeStandard(uuid);
    aggregate.commit();
    return;
  }
}
