import { ICommand } from '@nestjs/cqrs';
import { StandardDto } from '../../../entities/standard/standard.dto';

export class AddStandardCommand implements ICommand {
  constructor(
    public standardPayload: StandardDto,
    public readonly clientHttpRequest: any,
  ) {}
}
