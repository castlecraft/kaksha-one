import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { StandardAggregateService } from '../../../aggregates/standard/standard-aggregate.service';
import { AddStandardCommand } from './add-standard.command';

@CommandHandler(AddStandardCommand)
export class AddStandardHandler implements ICommandHandler<AddStandardCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StandardAggregateService,
  ) {}
  async execute(command: AddStandardCommand) {
    const { standardPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const standard = await aggregate.addStandard(
      standardPayload,
      clientHttpRequest,
    );
    aggregate.commit();
    return standard;
  }
}
