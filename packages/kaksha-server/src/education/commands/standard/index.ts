import { AddStandardHandler } from './add-standard/add-standard.handler';
import { RemoveStandardHandler } from './remove-standard/remove-standard.handler';
import { UpdateStandardHandler } from './update-standard/update-standard.handler';

export const StandardCommandHandlers = [
  AddStandardHandler,
  RemoveStandardHandler,
  UpdateStandardHandler,
];
