import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { StandardAggregateService } from '../../../aggregates/standard/standard-aggregate.service';
import { UpdateStandardCommand } from './update-standard.command';

@CommandHandler(UpdateStandardCommand)
export class UpdateStandardHandler
  implements ICommandHandler<UpdateStandardCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StandardAggregateService,
  ) {}

  async execute(command: UpdateStandardCommand) {
    const { updatePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateStandard(updatePayload, clientHttpRequest);
    aggregate.commit();
    return updatePayload;
  }
}
