import { ICommand } from '@nestjs/cqrs';
import { UpdateStandardDto } from '../../../entities/standard/standard.dto';

export class UpdateStandardCommand implements ICommand {
  constructor(
    public readonly updatePayload: UpdateStandardDto,
    public readonly clientHttpRequest: any,
  ) {}
}
