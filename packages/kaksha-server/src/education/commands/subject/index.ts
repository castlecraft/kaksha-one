import { AddSubjectHandler } from './add-subject/add-subject.handler';
import { RemoveSubjectHandler } from './remove-subject/remove-subject.handler';
import { UpdateSubjectHandler } from './update-subject/update-subject.handler';

export const SubjectCommandHandlers = [
  AddSubjectHandler,
  RemoveSubjectHandler,
  UpdateSubjectHandler,
];
