import { OrganizationCommandHandlers } from './organization';
import { QuizCommandHandlers } from './quiz';
import { StandardCommandHandlers } from './standard';
import { SubjectCommandHandlers } from './subject';
import { TeacherCommandHandlers } from './teacher';
import { TopicCommandHandlers } from './topic';

export const EducationCommandManager = [
  ...StandardCommandHandlers,
  ...SubjectCommandHandlers,
  ...OrganizationCommandHandlers,
  ...TeacherCommandHandlers,
  ...QuizCommandHandlers,
  ...TopicCommandHandlers,
];
