import { AddQuizHandler } from './add-quiz/add-quiz.handler';
import { RemoveQuizHandler } from './remove-quiz/remove-quiz.handler';
import { UpdateQuizHandler } from './update-quiz/update-standard.handler';

export const QuizCommandHandlers = [
  AddQuizHandler,
  RemoveQuizHandler,
  UpdateQuizHandler,
];
