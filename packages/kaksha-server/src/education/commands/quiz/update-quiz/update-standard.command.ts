import { ICommand } from '@nestjs/cqrs';
import { UpdateQuizDto } from '../../../entities/quiz/quiz.dto';

export class UpdateQuizCommand implements ICommand {
  constructor(
    public readonly updatePayload: UpdateQuizDto,
    public readonly clientHttpRequest: any,
  ) {}
}
