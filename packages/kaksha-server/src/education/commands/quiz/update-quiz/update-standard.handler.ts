import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { QuizAggregateService } from '../../../aggregates/quiz-aggregate/quiz-aggregate.service';
import { UpdateQuizCommand } from './update-standard.command';

@CommandHandler(UpdateQuizCommand)
export class UpdateQuizHandler implements ICommandHandler<UpdateQuizCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuizAggregateService,
  ) {}

  async execute(command: UpdateQuizCommand) {
    const { updatePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateQuiz(updatePayload, clientHttpRequest);
    aggregate.commit();
    return updatePayload;
  }
}
