import { ICommand } from '@nestjs/cqrs';
import { QuizDto } from '../../../entities/quiz/quiz.dto';

export class AddQuizCommand implements ICommand {
  constructor(
    public quizPayload: QuizDto,
    public readonly clientHttpRequest: any,
  ) {}
}
