import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { QuizAggregateService } from '../../../aggregates/quiz-aggregate/quiz-aggregate.service';
import { AddQuizCommand } from './add-quiz.command';

@CommandHandler(AddQuizCommand)
export class AddQuizHandler implements ICommandHandler<AddQuizCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuizAggregateService,
  ) {}
  async execute(command: AddQuizCommand) {
    const { quizPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const standard = await aggregate.addQuiz(quizPayload, clientHttpRequest);
    aggregate.commit();
    return standard;
  }
}
