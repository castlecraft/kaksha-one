import { ICommand } from '@nestjs/cqrs';

export class RemoveQuizCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
