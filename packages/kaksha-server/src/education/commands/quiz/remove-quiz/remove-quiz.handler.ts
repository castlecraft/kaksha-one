import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { QuizAggregateService } from '../../../aggregates/quiz-aggregate/quiz-aggregate.service';
import { RemoveQuizCommand } from './remove-quiz.command';

@CommandHandler(RemoveQuizCommand)
export class RemoveQuizHandler implements ICommandHandler<RemoveQuizCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: QuizAggregateService,
  ) {}

  async execute(command: RemoveQuizCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.removeQuiz(uuid);
    aggregate.commit();
    return;
  }
}
