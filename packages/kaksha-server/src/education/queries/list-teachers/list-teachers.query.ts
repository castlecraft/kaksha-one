import { IQuery } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { ListQueryDto } from '../../../common/list-query.dto';

export class ListTeachersQuery implements IQuery {
  constructor(
    public readonly query: ListQueryDto,
    public readonly token: TokenCache,
  ) {}
}
