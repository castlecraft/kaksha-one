import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { ListTeachersQuery } from './list-teachers.query';

@QueryHandler(ListTeachersQuery)
export class ListTeachersHandler implements IQueryHandler<ListTeachersQuery> {
  constructor(private readonly teacher: TeacherService) {}
  async execute(requestQuery: ListTeachersQuery) {
    const { query } = requestQuery;
    return await this.teacher.paginate(
      query.offset,
      query.limit,
      undefined,
      undefined,
    );
  }
}
