import { FetchOrganizationHandler } from './fetch-organization/fetch-organization.handler';
import { FetchTeacherHandler } from './fetch-teacher/fetch-teacher.handler';
import { ListTeachersHandler } from './list-teachers/list-teachers.handler';

export const EducationQueryHandlers = [
  FetchOrganizationHandler,
  ListTeachersHandler,
  FetchTeacherHandler,
];
