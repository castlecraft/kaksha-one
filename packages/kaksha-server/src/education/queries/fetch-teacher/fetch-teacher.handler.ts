import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';
import { FetchTeacherQuery } from './fetch-teacher.query';

@QueryHandler(FetchTeacherQuery)
export class FetchTeacherHandler implements IQueryHandler {
  constructor(private readonly aggregate: TeacherAggregateService) {}

  async execute(query: FetchTeacherQuery) {
    const { teacher, token } = query;
    return await this.aggregate.fetchTeacherForRoles(teacher, token);
  }
}
