import { IQuery } from '@nestjs/cqrs';
import { Teacher } from '../../entities/teacher/teacher.entity';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';

export class FetchTeacherQuery implements IQuery {
  constructor(
    public readonly teacher: Teacher,
    public readonly token: TokenCache,
  ) {}
}
