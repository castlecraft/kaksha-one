import { Test, TestingModule } from '@nestjs/testing';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { KakshaGateway } from './kaksha.gateway';

describe('KakshaGateway', () => {
  let gateway: KakshaGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        KakshaGateway,
        { provide: TokenCacheService, useValue: {} },
        { provide: ServerSettingsService, useValue: {} },
      ],
    }).compile();

    gateway = module.get<KakshaGateway>(KakshaGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
