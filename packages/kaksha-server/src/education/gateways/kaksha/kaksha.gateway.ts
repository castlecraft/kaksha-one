import { Req } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway(8000)
export class KakshaGateway {
  users: Array<string> = [];
  @WebSocketServer() wss: Server;
  @SubscribeMessage('UserEntered')
  // @UseGuards(TokenGuard)
  handleMessage(client: Socket, @MessageBody() data: any, @Req() req): void {
    if (!this.users.includes(data)) {
      this.users.push(data);
    }
    this.wss.emit('messageToCLient', this.users);
  }

  @SubscribeMessage('UserLeaved')
  handleUserLeaved(client: Socket, @MessageBody() data: any, @Req() req): void {
    this.users.splice(this.users.indexOf(data), 1);
    this.wss.emit('messageToDisconnect', this.users);
  }
}
