import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { EducationEntities, EducationEntityServices } from './entities';
import { OrganizationController } from './controllers/organization/organization.controller';
import { StandardController } from './controllers/standard/standard.controller';
import { SubjectController } from './controllers/subject/subject.controller';
import { StudentController } from './controllers/student/student.controller';
import { TeacherController } from './controllers/teacher/teacher.controller';
import { EducationQueryHandlers } from './queries';
import { EducationEventHandlers } from './events';
import { EducationAggregates } from './aggregates';
import { EducationPolicies } from './policies';
import { EducationCommandManager } from './commands';
import { QuizController } from './controllers/quiz/quiz.controller';
import { EducationGuards } from './guards';
import { TopicController } from './controllers/topic/topic.controller';
import { KakshaGateway } from './gateways/kaksha/kaksha.gateway';

@Module({
  imports: [
    TypeOrmModule.forFeature(EducationEntities),
    HttpModule,
    CqrsModule,
  ],
  providers: [
    ...EducationGuards,
    ...EducationCommandManager,
    ...EducationEntityServices,
    ...EducationQueryHandlers,
    ...EducationEventHandlers,
    ...EducationAggregates,
    ...EducationPolicies,
    KakshaGateway,
  ],
  exports: [...EducationEntityServices],
  controllers: [
    OrganizationController,
    StandardController,
    SubjectController,
    StudentController,
    TeacherController,
    QuizController,
    TopicController,
  ],
})
export class EducationModule {}
