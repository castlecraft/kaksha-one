import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { TeacherService } from '../entities/teacher/teacher.service';

@Injectable()
export class TeacherGuard implements CanActivate {
  constructor(private readonly teacher: TeacherService) {}

  async canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const teacher = await this.teacher.findOne({ email: req.token.email });
    if (teacher && teacher.email === req.token.email) {
      return true;
    }
    return false;
  }
}
