import { OrganizationAggregateService } from './organization-aggregate/organization-aggregate.service';
import { StandardAggregateService } from './standard/standard-aggregate.service';
import { SubjectAggregateService } from './subject/subject-aggregate.service';
import { TeacherAggregateService } from './teacher-aggregate/teacher-aggregate.service';
import { QuizAggregateService } from './quiz-aggregate/quiz-aggregate.service';
import { TopicAggregateService } from './topic-aggregate/topic-aggregate.service';

export const EducationAggregates = [
  SubjectAggregateService,
  OrganizationAggregateService,
  TeacherAggregateService,
  StandardAggregateService,
  QuizAggregateService,
  TopicAggregateService,
];
