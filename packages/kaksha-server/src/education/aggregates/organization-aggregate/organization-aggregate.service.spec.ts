import { Test, TestingModule } from '@nestjs/testing';
import { OrganizationService } from '../../entities/organization/organization.service';
import { OrganizationAggregateService } from './organization-aggregate.service';

describe('OrganizationAggregateService', () => {
  let service: OrganizationAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrganizationAggregateService,
        { provide: OrganizationService, useValue: {} },
      ],
    }).compile();

    service = module.get<OrganizationAggregateService>(
      OrganizationAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
