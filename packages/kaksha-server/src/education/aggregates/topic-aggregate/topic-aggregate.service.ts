import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import {
  TopicDto,
  UpdateTopicDto,
} from '../../../education/entities/topic/topic.dto';
import { Topic } from '../../entities/topic/topic.entity';
import { TopicService } from '../../entities/topic/topic.service';

@Injectable()
export class TopicAggregateService extends AggregateRoot {
  constructor(private readonly topicService: TopicService) {
    super();
  }

  async addTopic(
    topicPayload: TopicDto,
    clientHttpReq: Request & { token: TokenCache },
  ) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    topic.createdBy = clientHttpReq.token.name;
    topic.createdByEmail = clientHttpReq.token.email;
    topic.createdOn = new Date();
    await this.topicService.insertOne(topic);
  }

  async updateTopic(
    updatetopicPayload: UpdateTopicDto,
    clientHttpReq: Request,
  ) {
    return await this.topicService.updateOne(
      { uuid: updatetopicPayload.uuid },
      { $set: updatetopicPayload },
    );
  }

  async removeTopic(uuid: string) {
    return await this.topicService.deleteOne({ uuid });
  }

  async listTopic(offset, limit, sort, filter) {
    return await this.topicService.paginate(offset, limit, filter, sort);
  }

  async getTopic(uuid: string) {
    return await this.topicService.findOne({ uuid });
  }

  async getAllTopics(subjectName: string, standardName: string) {
    return await this.topicService.find({
      subject: subjectName,
      standard: standardName,
    });
  }
}
