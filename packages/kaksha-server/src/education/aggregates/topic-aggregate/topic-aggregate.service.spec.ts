import { Test, TestingModule } from '@nestjs/testing';
import { TopicService } from '../../entities/topic/topic.service';
import { TopicAggregateService } from './topic-aggregate.service';

describe('TopicAggregateService', () => {
  let service: TopicAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicAggregateService,
        {
          provide: TopicService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicAggregateService>(TopicAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
