import { Test, TestingModule } from '@nestjs/testing';
import { QuizAggregateService } from './quiz-aggregate.service';
import { QuizService } from '../../entities/quiz/quiz.service';

describe('QuizAggregateService', () => {
  let service: QuizAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuizAggregateService,
        {
          provide: QuizService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuizAggregateService>(QuizAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
