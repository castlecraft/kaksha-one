import { Injectable } from '@nestjs/common';
import { QuizService } from '../../entities/quiz/quiz.service';
import { Quiz } from '../../entities/quiz/quiz.entity';
import { QuizDto, UpdateQuizDto } from '../../entities/quiz/quiz.dto';
import { AggregateRoot } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';

@Injectable()
export class QuizAggregateService extends AggregateRoot {
  constructor(private readonly quizService: QuizService) {
    super();
  }

  async addQuiz(
    quizPayload: QuizDto,
    clientHttpReq: Request & { token: TokenCache },
  ) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    quiz.createdBy = clientHttpReq.token.name;
    quiz.createdByEmail = clientHttpReq.token.email;
    quiz.createdOn = new Date();
    await this.quizService.insertOne(quiz);
    return quiz;
  }

  async updateQuiz(updateQuizPayload: UpdateQuizDto, clientHttpReq: Request) {
    return await this.quizService.updateOne(
      { uuid: updateQuizPayload.uuid },
      { $set: updateQuizPayload },
    );
  }

  async removeQuiz(uuid: string) {
    return await this.quizService.deleteOne({ uuid });
  }

  async listQuiz(offset, limit, sort, filter) {
    return await this.quizService.paginate(offset, limit, filter, sort);
  }

  async getQuiz(uuid: string) {
    return await this.quizService.findOne({ uuid });
  }
}
