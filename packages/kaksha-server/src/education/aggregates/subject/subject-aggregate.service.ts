import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Request } from 'express';
import { Subject } from '../../entities/subject/subject.entity';
import {
  SubjectDto,
  UpdateSubjectDto,
} from '../../entities/subject/subject.dto';
import { SubjectService } from '../../entities/subject/subject.service';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';

@Injectable()
export class SubjectAggregateService extends AggregateRoot {
  constructor(private readonly subjectService: SubjectService) {
    super();
  }

  async addSubject(
    subjectPayload: SubjectDto,
    clientHttpReq: Request & { token: TokenCache },
  ) {
    const subject = new Subject();
    Object.assign(subject, subjectPayload);
    subject.createdBy = clientHttpReq.token.name;
    subject.createdByEmail = clientHttpReq.token.email;
    subject.createdOn = new Date();
    await this.subjectService.insertOne(subject);
  }

  async updateSubject(
    updateSubjectPayload: UpdateSubjectDto,
    clientHttpReq: Request,
  ) {
    return await this.subjectService.updateOne(
      { uuid: updateSubjectPayload.uuid },
      { $set: updateSubjectPayload },
    );
  }

  async removeSubject(uuid: string) {
    return await this.subjectService.deleteOne({ uuid });
  }

  async listSubject(offset, limit, sort, filter) {
    return await this.subjectService.paginate(offset, limit, filter, sort);
  }

  async getSubject(uuid: string) {
    return await this.subjectService.findOne({
      $or: [{ uuid }, { name: uuid }],
    });
  }

  async getAllSubjects(standardName: string) {
    return await this.subjectService.find({ standard: standardName });
  }

  async search(value: string, offset, limit, filter) {
    let where = filter;
    where = value
      ? {
          $or: [
            { standard: { $regex: value, $options: 'i' } },
            { name: { $regex: value, $options: 'i' } },
          ],
        }
      : where;
    return await this.subjectService.find({
      where,
      take: Number(limit) || 10,
      skip: Number(offset) || 0,
    });
  }
}
