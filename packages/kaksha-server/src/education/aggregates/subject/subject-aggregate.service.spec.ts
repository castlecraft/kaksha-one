import { Test, TestingModule } from '@nestjs/testing';
import { SubjectService } from '../../entities/subject/subject.service';
import { SubjectAggregateService } from './subject-aggregate.service';

describe('SubjectAggregateService', () => {
  let service: SubjectAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SubjectAggregateService,
        {
          provide: SubjectService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SubjectAggregateService>(SubjectAggregateService);
  });
  SubjectAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
