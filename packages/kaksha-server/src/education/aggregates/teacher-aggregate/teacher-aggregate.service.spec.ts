import { Test, TestingModule } from '@nestjs/testing';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { ValidateOrganizationAdminService } from '../../policies/validate-organization-admin/validate-organization-admin.service';
import { TeacherAggregateService } from './teacher-aggregate.service';

describe('TeacherAggregateService', () => {
  let service: TeacherAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherAggregateService,
        { provide: TeacherService, useValue: {} },
        { provide: ValidateOrganizationAdminService, useValue: {} },
      ],
    }).compile();

    service = module.get<TeacherAggregateService>(TeacherAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
