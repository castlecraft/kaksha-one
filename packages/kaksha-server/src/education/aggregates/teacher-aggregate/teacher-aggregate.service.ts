import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { TeacherDto } from '../../entities/teacher/teacher.dto';
import { Teacher } from '../../entities/teacher/teacher.entity';
import { TeacherService } from '../../entities/teacher/teacher.service';
import { TeacherAddedEvent } from '../../events/teacher-added/teacher-added.event';
import { UpdateTeacherDto } from '../../entities/teacher/update-teacher.dto';
import { TeacherUpdatedEvent } from '../../events/teacher-updated/teacher-updated.event';
import { TeacherDeletedEvent } from '../../events/teacher-deleted/teacher-deleted.event';

@Injectable()
export class TeacherAggregateService extends AggregateRoot {
  constructor(private readonly teacher: TeacherService) {
    super();
  }

  async addTeacher(payload: TeacherDto, token: TokenCache) {
    const teacher = await this.teacher.findOne({ email: payload.email });

    if (teacher) {
      throw new BadRequestException({
        TEACHER_EXISTS: payload.email,
      });
    }
    const response = { ...new Teacher(), ...payload } as Teacher;
    this.apply(new TeacherAddedEvent(this.setFullName(response), token));

    return response;
  }

  async updateTeacher(
    uuid: string,
    payload: UpdateTeacherDto,
    token: TokenCache,
  ) {
    const teacher = await this.teacher.findOne({ uuid });
    if (!teacher) {
      throw new NotFoundException({ TEACHER_NOT_FOUND: uuid });
    }
    const response = {
      ...teacher,
      ...this.setFullName(payload as Teacher),
    } as Teacher;
    this.apply(new TeacherUpdatedEvent(response, token));
    return response;
  }

  setFullName(response: Teacher) {
    if (response.firstName) {
      response.name = response.firstName;
    }

    if (response.middleName) {
      if (response.name) {
        response.name += ' ' + response.middleName;
      }
    }

    if (response.lastName) {
      if (response.name) {
        response.name += ' ' + response.lastName;
      }
    }

    return response;
  }

  async fetchTeacherForRoles(query: Teacher, token: TokenCache) {
    const teacher = await this.teacher.findOne(query);
    if (!teacher) {
      throw new NotFoundException({
        TEACHER_NOT_FOUND: query.email || query.uuid,
      });
    }
    return teacher;
  }

  async deleteTeacher(uuid: string, token: TokenCache) {
    const teacher = await this.teacher.findOne({ uuid });
    if (!teacher) {
      throw new NotFoundException({ TEACHER_NOT_FOUND: uuid });
    }
    this.apply(new TeacherDeletedEvent(teacher, token));
    return true;
  }
}
