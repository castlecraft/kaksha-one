import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Standard } from '../../entities/standard/standard.entity';
import {
  StandardDto,
  UpdateStandardDto,
} from '../../entities/standard/standard.dto';
import { StandardService } from '../../entities/standard/standard.service';
import { ClientHttpRequestInterface } from '../../../common/list-query.dto';

@Injectable()
export class StandardAggregateService extends AggregateRoot {
  constructor(private readonly standardService: StandardService) {
    super();
  }

  async addStandard(
    standardPayload: StandardDto,
    clientHttpReq: ClientHttpRequestInterface,
  ) {
    const standard = new Standard();
    Object.assign(standard, standardPayload);
    standard.createdBy = clientHttpReq.token.name;
    standard.createdByEmail = clientHttpReq.token.email;
    standard.createdOn = new Date();
    await this.standardService.insertOne(standard);
    return standard;
  }

  async updateStandard(
    updateStandardPayload: UpdateStandardDto,
    clientHttpReq: ClientHttpRequestInterface,
  ) {
    return await this.standardService.updateOne(
      { uuid: updateStandardPayload.uuid },
      { $set: updateStandardPayload },
    );
  }

  async removeStandard(uuid: string) {
    return await this.standardService.deleteOne({ uuid });
  }

  async listStandard(offset, limit, sort, filter) {
    return await this.standardService.paginate(offset, limit, filter, sort);
  }

  async getStandard(uuid: string) {
    return await this.standardService.findOne({
      $or: [{ uuid }, { name: uuid }],
    });
  }
}
