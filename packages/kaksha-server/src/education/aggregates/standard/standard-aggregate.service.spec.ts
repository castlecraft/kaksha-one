import { Test, TestingModule } from '@nestjs/testing';
import { StandardService } from '../../entities/standard/standard.service';
import { StandardAggregateService } from './standard-aggregate.service';

describe('StandardAggregateService', () => {
  let service: StandardAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StandardAggregateService,
        {
          provide: StandardService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StandardAggregateService>(StandardAggregateService);
  });
  StandardAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
