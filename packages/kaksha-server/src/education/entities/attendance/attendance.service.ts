import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Attendance } from './attendance.entity';

@Injectable()
export class AttendanceService {
  constructor(
    @InjectRepository(Attendance)
    private readonly repo: MongoRepository<Attendance>,
  ) {}

  async insertOne(params) {
    return await this.repo.insertOne(params);
  }

  async insertMany(params, options?) {
    options = options ? options : { ordered: false };
    return await this.repo.insertMany(params);
  }

  async find(params?): Promise<Attendance[]> {
    return await this.repo.find(params);
  }

  async findOne(params) {
    return await this.repo.findOne(params);
  }

  async updateOne(query, params) {
    return await this.repo.updateOne(query, params);
  }

  async updateMany(query, params) {
    return await this.repo.updateMany(query, params);
  }

  async count() {
    return await this.repo.count();
  }

  async paginate(skip: number, take: number, where: any, order: any) {
    skip = Number(skip);
    take = Number(take);
    order = { _id: order?.toUpperCase() || 'ASC' };

    const docs = await this.repo.find({
      skip,
      take,
      order,
      where,
    });

    const length = await this.repo.count(where);
    return { docs, length, offset: skip ? skip : 0 };
  }

  async deleteMany(params) {
    return await this.repo.deleteMany(params);
  }

  async deleteOne(params) {
    return await this.repo.deleteOne(params);
  }
}
