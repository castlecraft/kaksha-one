import { IsDateString, IsEmail, IsOptional, IsString } from 'class-validator';

export class UpdateTeacherDto {
  @IsDateString()
  @IsOptional()
  leavingDate: Date;

  @IsString()
  firstName: string;

  @IsString()
  @IsOptional()
  middleName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsEmail()
  email: string;
}
