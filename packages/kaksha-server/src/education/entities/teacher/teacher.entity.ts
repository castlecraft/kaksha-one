import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { Subject } from '../subject/subject.entity';

@Entity()
export class Teacher extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  joiningDate: Date;

  @Column()
  leavingDate: Date;

  @Column()
  firstName: string;

  @Column()
  middleName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  name: string;

  @Column()
  subjects: Subject[];

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
