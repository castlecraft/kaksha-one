import { ArrayMinSize, IsArray, IsOptional, IsString } from 'class-validator';

export class StandardDto {
  @IsString()
  name: string;

  @IsArray()
  @ArrayMinSize(1)
  divisions: string[];
}

export class UpdateStandardDto {
  @IsString()
  uuid: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  divisions: string[];
}
