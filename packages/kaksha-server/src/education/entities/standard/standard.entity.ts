import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Standard extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column({ unique: true })
  name: string;

  @Column()
  divisions: string[];

  @Column()
  createdBy: string;

  @Column()
  createdByEmail: string;

  @Column()
  createdOn: Date;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
