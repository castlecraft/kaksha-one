import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Subject extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  standard: string; // name of Standard

  @Column()
  name: string;

  @Column()
  createdBy: string;

  @Column()
  createdOn: Date;

  @Column()
  createdByEmail: string;

  @Column()
  subjectState: { State };

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}

export class State {
  [key: string]: {
    lastTopic: string;
    currentTopic: string;
    nextTopic: string;
    totalTopics: number;
    completedTopics: number;
  };
}
