import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { State } from './subject.entity';

export class SubjectDto {
  @IsNotEmpty()
  @IsString()
  standard: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  subjectState: State;
}

export class UpdateSubjectDto {
  @IsNotEmpty()
  @IsString()
  uuid: string;

  @IsOptional()
  @IsString()
  standard: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  subjectState: State;
}
