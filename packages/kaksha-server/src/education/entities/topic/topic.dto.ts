import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class TopicDto {
  @IsOptional()
  @IsString()
  uuid: string;

  @IsOptional()
  @IsString()
  subject: string;

  @IsOptional()
  @IsString()
  standard: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  createdBy: string;

  @IsOptional()
  @IsString()
  createdOn: string;

  @IsOptional()
  @IsString()
  createdByEmail: string;
}

export class UpdateTopicDto {
  @IsNotEmpty()
  @IsString()
  uuid: string;

  @IsOptional()
  @IsString()
  subject: string;

  @IsOptional()
  @IsString()
  standard: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  createdBy: string;

  @IsOptional()
  @IsString()
  createdOn: string;

  @IsOptional()
  @IsString()
  createdByEmail: string;
}
