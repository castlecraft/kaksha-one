import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class QuizDto {
  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  type: string;

  @IsString()
  @IsOptional()
  subject: string;

  @IsString()
  @IsOptional()
  standard: string;

  @IsString()
  @IsOptional()
  createdBy: string;

  @IsString()
  @IsOptional()
  createdOn: string;

  @IsString()
  @IsOptional()
  createdByEmail: string;

  @IsOptional()
  questions: any;

  @IsOptional()
  answer: any;
}

export class UpdateQuizDto {
  @IsString()
  @IsNotEmpty()
  uuid: string;

  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  type: string;

  @IsString()
  @IsOptional()
  subject: string;

  @IsString()
  @IsOptional()
  standard: string;

  @IsString()
  @IsOptional()
  createdBy: string;

  @IsString()
  @IsOptional()
  createdOn: string;

  @IsString()
  @IsOptional()
  createdByEmail: string;

  @IsOptional()
  questions: any;

  @IsString()
  @IsOptional()
  answer: any;
}
