import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class Quiz extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  date: Date;

  @Column()
  title: string;

  @Column()
  type: string;

  @Column()
  subject: string;

  @Column()
  standard: string;

  @Column()
  createdBy: string;

  @Column()
  createdOn: Date;

  @Column()
  createdByEmail: string;

  @Column()
  questions: any;

  @Column()
  answer: any;

  @Column()
  name: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
