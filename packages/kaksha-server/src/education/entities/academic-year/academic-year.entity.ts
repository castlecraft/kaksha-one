import { Entity, BaseEntity, ObjectIdColumn, Column, ObjectID } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

@Entity()
export class AcademicYear extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  fromYear: string; // 2019

  @Column()
  toYear: string; // 2020

  @Column()
  name: string; // 2019-2020

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
