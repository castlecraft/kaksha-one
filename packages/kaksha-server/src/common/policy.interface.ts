export interface IPolicy {
  validate: (...args) => Promise<any>;
}
