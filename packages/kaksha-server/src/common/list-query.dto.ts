import { IsOptional } from 'class-validator';
import { TokenCache } from '../auth/entities/token-cache/token-cache.entity';

export interface ClientHttpRequestInterface {
  token: TokenCache;
}

export class ListQueryDto {
  @IsOptional()
  offset: number;

  @IsOptional()
  limit: number;

  @IsOptional()
  search: string;

  @IsOptional()
  // @IsInstance(QuerySort)
  sort: string;

  @IsOptional()
  filter_query: string;
}

export class GetAllTopicsDto {
  @IsOptional()
  standardName: string;

  @IsOptional()
  subjectName: string;
}
