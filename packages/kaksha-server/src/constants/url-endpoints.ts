export const INFO_ENDPOINT = '/info';
export const SERVICE_TYPE_CREATE_ENDPOINT = '/service_type/v1/create';
export const SERVICE_REGISTER_ENDPOINT = '/service/v1/register';
export const VERIFY_USER_ENDPOINT = '/auth/verify_user';
export const USER_SIGN_UP_ENDPOINT = '/user_signup/v1/email';
